﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewBoxText
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public List<string> FewItems { get; set; } = new List<string>()
        {
            "hallo","hallo"
        };
        public List<string> ManyItems { get; set; } = new List<string>()
        {
            "hallo","hallo","hallo","hallo","hallo","hallo","hallo","hallo"
        };

    }
}
